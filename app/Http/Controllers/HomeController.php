<?php

namespace App\Http\Controllers;

use App\Models\User;
use Dflydev\DotAccessData\Data;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
