<header id="masthead" class="site-header">
    <div class="site-branding">
        <h1 class="site-title"><a href="index.html" rel="home">Moschino</a></h1>
        <h2 class="site-description">Minimalist Portfolio HTML Template</h2>
    </div>
    <nav id="site-navigation" class="main-navigation">
        <button class="menu-toggle">Menu</button>
        <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
        <div class="menu-menu-1-container">
            <ul id="menu-menu-1" class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{route('about')}}">About</a></li>
                <li><a href="{{route('shop')}}">Shop</a></li>
                <li><a href="{{route('blog')}}">Blog</a></li>
                <li><a href="{{route('contact')}}">Contact</a></li>

                @guest
                    <li><a href="{{route('login')}}">Login</a></li>
                @endguest

                @auth
                    <li><a href="{{route('logout')}}">Logout</a></li>
                @endauth
            </ul>
        </div>
    </nav>
</header>
